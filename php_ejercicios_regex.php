
<?php
// ADRIAN PEREZ DE LA VEGA

//Realizar una expresión regular que detecte emails correctos.
	$check = "ejemplocorreo@dominio.com";
	$res = preg_match('/^[a-z0-9._-]+@[a-z0-9_-]+\.[a-z0-9]{2,4}$/i',$check);
	echo $res; 

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
	print("\n");
	$check = "A5BCJE123458754FGJ";
	$res = preg_match('/^[A-J1-8]{18,18}$/i',$check);
	echo $res;


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
	print("\n");
	$check = "ejemplodeunapalabradecincuentaomascaracteresformadoporletras    otroejemplodeunapalabradecincuentaomascaracteresformadoporletras";
	$res = preg_match('/^([a-zA-Z]{51,}(\s+[a-zA-Z]{51,})*)$/i',$check);
	echo $res;

//Crea una funcion para escapar los simbolos especiales.
	print("\n");
	$check = '$..*{{{] $^^*';
	$res = preg_match('/^[\.\[\]\^\*\+\?\{\}\|\/\$\s]+$/i',$check);
	echo $res;

//Crear una expresion regular para detectar números decimales.

	print("\n");
	$check = "126.14";
	$res = preg_match('/^([0-9]+(\.[0-9]+|[0-9]*))$/i',$check);
	echo $res;
?>